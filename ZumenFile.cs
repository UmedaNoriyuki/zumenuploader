﻿namespace ZumenUploder
{
    public class ZumenFile
    {
        public string FileName { get; set; }
        public string FileSize { get; set; }
    }
}