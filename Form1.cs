﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Windows.Forms;
using Npgsql;

namespace ZumenUploder
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string work1 = string.Empty;
        string work2 = string.Empty;

        List<ZumenFile> zfList { get; set; }

        /// <summary>
        /// ロード時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                string[] command = Environment.GetCommandLineArgs();

                string csvPath = "";
                //アップロード先のURI
                string uri = "";
                if (command.Length > 1 && command[1] == "est")
                {
                    // 読み込みたいCSVファイルのパスを指定して開く
                    csvPath = @"\\repro-server\repro\FtpZumen\estimateZumen.csv";
                    uri = "ftp://nakamurakikai.co.jp//public_html/repro/estimateZumen/";
                }
                else if (command.Length > 1 && command[1] == "ord")
                {
                    // 読み込みたいCSVファイルのパスを指定して開く
                    csvPath = @"\\repro-server\repro\FtpZumen\orderZumen.csv";
                    uri = "ftp://nakamurakikai.co.jp//public_html/repro/orderZumen/";
                }
                else
                {
                    this.Close();
                }

                // 読み込みたいCSVファイルのパスを指定して開く
                StreamReader sr = new StreamReader(csvPath);

                zfList = new List<ZumenFile>();
                
                // 図面CSVファイルを読み込みリストに追加
                while (!sr.EndOfStream)
                {
                    // CSVファイルの一行を読み込む
                    string line = sr.ReadLine();
                    // 読み込んだ一行をカンマ毎に分けて配列に格納する
                    string[] values = line.Split(',');
                    ZumenFile zf = new ZumenFile();
                    zf.FileName = values[0];
                    zf.FileSize = values[1];
                    zfList.Add(zf);
                }
                sr.Close();

                var conn = Properties.Settings.Default.connectionString;
                var cmd = new NpgsqlCommand();

                using (var con = new NpgsqlConnection(conn))
                {
                    con.Open();

                    var sqlCmdTxt = "";
                    if (command.Length > 1 && command[1] == "est")
                    {
                        sqlCmdTxt = "select" +
                                    "  図面パス" +
                                    "  ,区分 || 見積依頼番号 || TO_CHAR(行番号, 'FM00000') as ファイル名" +
                                    " from 見積依頼ビュー " +
                                    " WHERE 図面パス is not null" +
                                    "   AND (区分 in(2,3) " +
                                    // 翔南産業のみ材料図面添付
                                    "   OR (区分 = 4 AND 仕入先コード = 15200)) ";
                    }
                    else if (command.Length > 1 && command[1] == "ord")
                    {
                        sqlCmdTxt = "select" +
                                    "  t.図面パス as 図面パス" +
                                    "  ,t.区分 || t.注文書番号 || TO_CHAR(t.注文書行番号, 'FM00000') as ファイル名" +
                                    "  from (SELECT " +
                                    "    b.注文書番号 " +
                                    "    ,b.注文書行番号 " +
                                    "    ,2 as 区分 " +
                                    "    ,b.数量 " +
                                    "    ,b.検収数量 " +
                                    "    ,b.図面パス" +
                                    "    from 実績加工データ b " +
                                    "      JOIN 仕入先マスタ sm ON sm.仕入先コード = b.加工先コード" +
                                    "    WHERE b.注文書番号 IS NOT NULL AND sm.社内区分 = 0 AND b.検収区分 = 0 AND b.遅延区分 = 0" +
                                    "  UNION" +
                                    "    SELECT " +
                                    "    c.注文書番号 " +
                                    "    ,c.注文書行番号 " +
                                    "    ,3 as 区分 " +
                                    "    ,c.数量 " +
                                    "    ,c.検収数量 " +
                                    "    ,b.図面パス" +
                                    "    from 実績工程データ c " +
                                    "      JOIN 仕入先マスタ sm ON sm.仕入先コード = c.加工先コード" +
                                    "      LEFT JOIN 実績加工データ b ON c.\"オーダーNO\"::text = b.\"オーダーNO\"::text AND c.グループ::text = b.グループ::text AND c.連番 = b.連番" +
                                    "    WHERE c.注文書番号 IS NOT NULL AND sm.社内区分 = 0 AND c.注文区分 = 5 AND c.遅延区分 = 0" +
                                    "  UNION" +
                                    "    SELECT " +
                                    "    d.注文書番号 " +
                                    "    ,d.注文書行番号 " +
                                    "    ,4 as 区分 " +
                                    "    ,d.数量 " +
                                    "    ,d.検収数量 " +
                                    "    ,b.図面パス " +
                                    "    from 実績材料データ d " +
                                    "      JOIN 仕入先マスタ sm ON sm.仕入先コード = d.仕入先コード" +
                                    "      LEFT JOIN 実績加工データ b ON d.\"オーダーNO\"::text = b.\"オーダーNO\"::text AND d.グループ::text = b.グループ::text AND d.連番 = b.連番" +
                                    "    WHERE d.注文書番号 IS NOT NULL AND sm.社内区分 = 0 AND d.出庫区分 = 0 AND d.遅延区分 = 0 AND sm.材料図面添付区分 = 1 " +
                                    // 翔南産業のみ材料図面添付
                                    "      AND d.仕入先コード = 15200) t" +
                                    "  WHERE t.図面パス is not null AND t.数量 - t.検収数量 > 0";
                    }

                    cmd = new NpgsqlCommand(sqlCmdTxt, con);

                    var result = cmd.ExecuteReader();
                    bool retryFlg = false;
                    while (result.Read())
                    {
                        try
                        {
                            var zumenPath = ConvertToString(result["図面パス"]);
                            work1 = zumenPath;
                            if (zumenPath == null || !System.IO.File.Exists(zumenPath))
                            {
                                continue;
                            }
                            var fileName = ConvertToString(result["ファイル名"]);
                            work2 = fileName;
                            //TS-XEL980の図面ファイルをサーバー上にアップロードする
                            FileUpload(zumenPath, fileName, uri);
                        }
                        catch(Exception exp)
                        {
                            // エラーログ作成
                            string errorMes = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " ";
                            if (!retryFlg)
                            {
                                errorMes += "初回エラー" + Environment.NewLine;
                            }
                            else
                            {
                                errorMes += "リトライエラー" + Environment.NewLine;
                            }
                            errorMes += exp.Message + Environment.NewLine;
                            errorMes += exp.GetType().FullName + Environment.NewLine + exp.StackTrace + Environment.NewLine;
                            string mesPath = @"\\repro-server\repro\FtpZumen\" + DateTime.Now.ToString("yyyyMMdd") + "_error.log";
                            System.IO.File.AppendAllText(mesPath, errorMes);
                            if (!retryFlg)
                            {
                                retryFlg = true;
                            }
                            else
                            {
                                // エラーメール送信
                                sendErrorMail();
                                // 再度エラーになったときアプリを閉じる
                                this.Close();
                            }
                        }
                    }
                }

                Uri u = new Uri(uri);

                //FtpWebRequestの作成
                System.Net.FtpWebRequest ftpReq = (System.Net.FtpWebRequest)
                    System.Net.WebRequest.Create(u);
                //ログインユーザー名とパスワードを設定
                ftpReq.Credentials = new System.Net.NetworkCredential("isu5014", "2a8216c60c");
                // ファイル名一覧の取得
                ftpReq.Method = WebRequestMethods.Ftp.ListDirectory;
                //要求の完了後に接続を閉じる
                ftpReq.KeepAlive = false;

                //FtpWebResponseを取得
                System.Net.FtpWebResponse ftpRes =
                    (System.Net.FtpWebResponse)ftpReq.GetResponse();

                //FTPサーバーから送信されたデータを取得
                sr = new System.IO.StreamReader(ftpRes.GetResponseStream());
                // ファイル名一覧
                string fileListStr = sr.ReadToEnd().Trim();
                string[] separator = new string[] { "\r\n" };
                //ファイル名一覧を配列にする
                string[] files = fileListStr.Split(separator, StringSplitOptions.None);
                //配列をリストに変換
                var ftpFiles = new List<string>();
                ftpFiles.AddRange(files);

                sr.Close();
                //FTP操作の終了
                ftpReq.Abort();
                //閉じる
                ftpRes.Close();

                //CSVファイルに書き込むときに使うEncoding
                System.Text.Encoding enc =
                    System.Text.Encoding.GetEncoding("Shift_JIS");

                //書き込むファイルを開く
                System.IO.StreamWriter sw =
                    new System.IO.StreamWriter(csvPath, false, enc);

                foreach (var zf in zfList)
                {
                    if (ftpFiles.Contains(zf.FileName + ".xdw"))
                    {
                        //"で囲む
                        string field = EncloseDoubleQuotesIfNeed(zf.FileName);
                        //フィールドを書き込む
                        sw.Write(field);
                        //カンマを書き込む
                        sw.Write(',');
                        //"で囲む
                        field = EncloseDoubleQuotesIfNeed(zf.FileSize);
                        //フィールドを書き込む
                        sw.Write(field);
                        //改行する
                        sw.Write("\r\n");
                    }
                    
                }
                sw.Close();
                
            }
            catch (Exception ex)
            {
                // エラーログ作成
                string errorMes = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") + " " + ex.Message + Environment.NewLine;
                errorMes += ex.GetType().FullName + Environment.NewLine + ex.StackTrace + Environment.NewLine;
                string mesPath = @"\\repro-server\repro\FtpZumen\" + DateTime.Now.ToString("yyyyMMdd") + "_error.log";
                System.IO.File.AppendAllText(mesPath, errorMes);
                // エラーメール送信
                sendErrorMail();
                this.Close();
            }

            this.Close();
        }

        /// <summary>
        /// エラーメール送信
        /// </summary>
        private void sendErrorMail()
        {
            //MailMessageの作成
            System.Net.Mail.MailMessage msg = new System.Net.Mail.MailMessage();
            //送信メールアドレス（メールアドレスはsystem@nakamurakikai.co.jpに統一する）
            msg.From = new System.Net.Mail.MailAddress("system@nakamurakikai.co.jp");
            //宛先
            msg.To.Add(new System.Net.Mail.MailAddress("umeda.noriyuki@nakamurakikai.co.jp"));

            //件名
            msg.Subject = "図面アップロード実行エラー通知";
            //本文
            string mesPath = @"\\repro-server\repro\FtpZumen\" + DateTime.Now.ToString("yyyyMMdd") + "_error.log";
            msg.Body = "図面アップロード時にエラーが発生しました。\n下記のログを参照してください。\n" + mesPath;

            msg.Priority = System.Net.Mail.MailPriority.Normal;
            //メールの配達が遅れたとき、失敗したとき、正常に配達されたときに通知する
            msg.DeliveryNotificationOptions =
                System.Net.Mail.DeliveryNotificationOptions.Delay |
                System.Net.Mail.DeliveryNotificationOptions.OnFailure |
                System.Net.Mail.DeliveryNotificationOptions.OnSuccess;

            // メールのアカウント、パスワードを設定
            System.Net.Mail.SmtpClient sc = new System.Net.Mail.SmtpClient();
            sc.Credentials = new System.Net.NetworkCredential("system@nakamurakikai.co.jp", "wGb1o10VX5");
            //SMTPサーバーなどを設定する
            sc.Host = "smtp.nakamurakikai.co.jp";
            sc.Port = 587;
            sc.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            //メッセージを送信する
            sc.Send(msg);

            //後始末
            msg.Dispose();
            //後始末（.NET Framework 4.0以降）
            sc.Dispose();
        }

        private string ConvertToString(object obj)
        {
            if (obj == null)
            {
                return string.Empty;
            }
            else
            {
                return obj.ToString();
            }
        }

        /// <summary>
        /// TS-XEL980の図面ファイルをサーバー上にアップロードする
        /// </summary>
        /// <param name="zumenPath"></param>
        /// <param name="fileName"></param>
        /// <param name="uri"></param>
        private void FileUpload(string zumenPath, string fileName, string uri)
        {
            //アップロードするファイルを開く
            System.IO.FileStream fs = new System.IO.FileStream(
                zumenPath, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite);
            string zfSize = "";
            int zfListIndex = zfList.FindIndex(x => x.FileName == fileName);
            if (zfListIndex != -1)
            {
                zfSize = zfList.Find(x => x.FileName == fileName).FileSize;
            }
            // 新規で追加された図面または図面が変更された場合アップロードを実行
            if (zfSize != fs.Length.ToString())
            {
                //アップロード先のURI
                Uri u = new Uri(uri + fileName + ".xdw");

                //FtpWebRequestの作成
                System.Net.FtpWebRequest ftpReq = (System.Net.FtpWebRequest)
                    System.Net.WebRequest.Create(u);
                //ログインユーザー名とパスワードを設定
                ftpReq.Credentials = new System.Net.NetworkCredential("isu5014", "2a8216c60c");
                //MethodにWebRequestMethods.Ftp.UploadFile("STOR")を設定
                ftpReq.Method = System.Net.WebRequestMethods.Ftp.UploadFile;
                //要求の完了後に接続を閉じる
                ftpReq.KeepAlive = false;
                //PASVモードを無効にする
                ftpReq.UsePassive = false;

                //ファイルをアップロードするためのStreamを取得
                System.IO.Stream reqStrm = ftpReq.GetRequestStream();

                //アップロードStreamに書き込む
                byte[] buffer = new byte[1024];
                while (true)
                {
                    int readSize = fs.Read(buffer, 0, buffer.Length);
                    if (readSize == 0)
                        break;
                    reqStrm.Write(buffer, 0, readSize);
                }

                ZumenFile zf = new ZumenFile();
                zf.FileName = fileName;
                zf.FileSize = fs.Length.ToString();
                // 新規で追加された図面または図面が変更された場合、リストに追加
                if (zfSize != fs.Length.ToString())
                {
                    // 図面が変更されている場合はリストから一旦削除
                    if (zfListIndex != -1)
                    {
                        zfList.RemoveAt(zfListIndex);
                    }
                    zfList.Add(zf);
                }

                fs.Close();
                reqStrm.Close();

                //FtpWebResponseを取得
                System.Net.FtpWebResponse ftpRes =
                    (System.Net.FtpWebResponse)ftpReq.GetResponse();
                //FTP操作の終了
                ftpReq.Abort();
                //閉じる
                ftpRes.Close();
            }
        }

        /// <summary>
        /// 必要ならば、文字列をダブルクォートで囲む
        /// </summary>
        private string EncloseDoubleQuotesIfNeed(string field)
        {
            if (NeedEncloseDoubleQuotes(field))
            {
                return EncloseDoubleQuotes(field);
            }
            return field;
        }

        /// <summary>
        /// 文字列をダブルクォートで囲む
        /// </summary>
        private string EncloseDoubleQuotes(string field)
        {
            if (field.IndexOf('"') > -1)
            {
                //"を""とする
                field = field.Replace("\"", "\"\"");
            }
            return "\"" + field + "\"";
        }

        /// <summary>
        /// 文字列をダブルクォートで囲む必要があるか調べる
        /// </summary>
        private bool NeedEncloseDoubleQuotes(string field)
        {
            return field.IndexOf('"') > -1 ||
                field.IndexOf(',') > -1 ||
                field.IndexOf('\r') > -1 ||
                field.IndexOf('\n') > -1 ||
                field.StartsWith(" ") ||
                field.StartsWith("\t") ||
                field.EndsWith(" ") ||
                field.EndsWith("\t");
        }
    }
}
